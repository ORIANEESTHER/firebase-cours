import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from './Screens/HomeScreen';
import LoginScreen from './Screens/LoginScreen';
import { onAuthStateChanged } from 'firebase/auth';
import { FIREBASE_APP } from './firebaseConfig';

const Stack = createStackNavigator();
const InnerStack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="LoginScreen">
        <Stack.Screen name="HomeScreen" component={HomeScreen} />
        <Stack.Screen name="LoginScreen" component={LoginScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};


export default App(){
  const [user,setUser]=React.useState(null)

  React.useEffect(() => {
    onAuthStateChanged(FIREBASE_APP , (user) => {
      console.log('user:',.0)
    })
  })

}

