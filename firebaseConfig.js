// Import the functions you need from the SDKs you need

import { initializeApp } from "firebase/app";

import { getAuth } from "firebase/auth";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional

const firebaseConfig = {
  apiKey: "AIzaSyDYQTAkn5AytPinEN5TW_9e3C274jgFrAc",

  authDomain: "fir-test-22acc.firebaseapp.com",

  projectId: "fir-test-22acc",
  
  storageBucket: "fir-test-22acc.appspot.com",

  messagingSenderId: "992139150205",

  appId: "1:992139150205:web:bd85413eea743b47944214",

  measurementId: "G-HDMVVT2D9B"
};

// Initialize Firebase

export const FIREBASE_APP = initializeApp(firebaseConfig);
export const  FIREBASE_AUTH = getAuth(FIREBASE_APP);