import React, { useState , useEffectffect } from 'react';
import { View, TextInput, Text, ActivityIndicator } from 'react-native';
 import { FIREBASE_AUTH } from '../firebaseConfig';
 import {ActivityIndicator} from 'react-native-web'

const LoginScreen = ({navigation}) => {
    const [email, setEmail] =React.useState("");
    const [loading, setLoading] =React.useState("");
    const [password, setPassword] = React.useState(false);
    const auth = FIREBASE_AUTH

    const signIn =async()=> {
        setLoading(true);
        try{
            const response =await signInWithEmailAndPassword(auth,email,password)
            console.log(response)
        }catch (error){
            console.log(error)
        }finally{
            setLoading(false)
        }
    }
    }

    const signUp =async () => {
        setLoading(true)
        try{
            const response = await auth.createUserWithAndPassword(email,password)
            console.log(response);
        }catch (error){
            console.log(error)
        }finally{
            setLoading(false)
        }
    }

    return (
        <View style={style.container}>
            <Text>LoginScreen</Text>
            <TextInput
                placeholder="Email"
                value={email}
                onChangeText={setEmail}  />
            <TextInput
                placeholder="Loading"
                value={loading}
                onChangeText={setLoading}
            />
            <TextInput
                placeholder="Password"
                value={password}
                onChangeText={setPassword}
                secureTextEntry={true} />
               cd   
        </View>
    );


export default LoginScreen;
